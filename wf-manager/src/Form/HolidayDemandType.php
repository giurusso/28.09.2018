<?php

namespace App\Form;

use App\DTO\DTOHolidayDemand;
use Doctrine\DBAL\Types\BooleanType;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HolidayDemandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("startDate", DateType::class, [
                'input' => 'datetime_immutable',
                'label' => 'Date de début',
            ])
            ->add("endDate", DateType::class, [
                'input' => 'datetime_immutable',
                'label' => 'Date de fin',
            ])
            ->add('comment', TextareaType::class,[
                'attr' => ['class' => 'form-control', 'id' => 'exampleTextarea', 'rows' => '5', 'cols' => '90', 'placeholder' => 'Commentaire facultatif'],
                'label' => false,
                'required' => false,
                'empty_data' => ''
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // Formulaire va avoir DTOHolidayDemand pour réceptacle
        $resolver->setDefault("data_class", DTOHolidayDemand::class);
    }
}
