<?php

namespace App\Form;

use App\DTO\DTOUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'label' => 'Email :',
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Le contenu des deux champs de mot de passe doit correspondre.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options' => array('label' => 'Mot de passe :'),
                'second_options' => array('label' => 'Mot de passe répété :'),
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Nom :',
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Prénom :',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // Formulaire va avoir DTOUser pour réceptacle
        $resolver->setDefault("data_class", DTOUser::class);
    }
}
