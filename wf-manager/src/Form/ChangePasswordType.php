<?php

namespace App\Form;


use App\DTO\DTOUser;
use phpDocumentor\Reflection\Types\String_;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ChangePasswordType
 * @package App\Form
 * @Security("is_granted('ROLE_AROLE_ADMINISTRATOR')")
 */
class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Le contenu des champs de mot de passe doit correspondre.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options' => array('label' => false),
                'second_options' => array('label' => false),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // Formulaire va avoir DTOUser pour réceptacle
        $resolver->setDefault("data_class", DTOUser::class);
    }

}