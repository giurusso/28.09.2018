<?php


namespace App\Form;


use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class HolidayDemandManagementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('acceptButton', SubmitType::class,[
                'label' => 'Accepter',
                'attr' => ['class'=>'btn btn-success'],
            ])
            ->add('refuseButton', SubmitType::class,[
                'label' => 'Refuser',
                'attr' => ['class'=>'btn btn-danger'],
            ])
            ->add('comment', TextareaType::class,[
                'attr' => ['class' => 'form-control', 'id' => 'exampleTextarea', 'rows' => '3', 'placeholder' => 'Commentaire facultatif'],
                'label' => false,
                'required' => false,
            ])
        ;
    }
}

