<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 10.08.2018
 * Time: 18:03
 */

namespace App\DataFixtures;

use App\Entity\Day;
use App\Entity\HolidayDemand;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;




class DemandsFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $this->loadDev($manager);
        $manager->flush();
    }

//    private function loadDemo(ObjectManager $manager)
//    {
//        $userRH = new User("jresponsable", "Jean", "Reponsable", false);
//        $user1 = new User("ptechnicien", "Pierre", "Technicien", true);
//        $user2 = new User("jconseiller", "Jacques", "Conseiller", true);
//        $manager->persist($user1);
//        $manager->persist($user2);
//        $manager->persist($userRH);
//    }


    private function loadDev(ObjectManager $manager)
    {

        //=========================================================================
        // Création des utilisateurs
        //=========================================================================

//        password_hash($plainString, PASSWORD_BCRYPT, ['cost' => self::COST])
//        $password = password_hash('pass', PASSWORD_BCRYPT, ['cost' => 12]);
        $password = 'pass2018';
        $user1 = new User('fdamiens@swiss.com', $password, 'Damiens', 'François');
        $user2 = new User('ajacquard@swiss.com', $password, 'Jacquard', 'Albert');
        $user3 = new User('jmuller@swiss.com', $password,'Müller', 'Jeanne');
        $user4 = new User('pleader@swiss.com', $password,'Leader', 'Paul');
        $user5 = new User('jsuper@swiss.com', $password,'Super', 'Jean');
        $user6 = new User('precteur@swiss.com', $password, 'Recteur', 'Pierre');
        $user7 = new User('admin', $password, 'Strateur', 'Alexandre');

        $user7->promoteToAdministrator();


        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->persist($user4);
        $manager->persist($user5);
        $manager->persist($user6);
        $manager->persist($user7);


        //=========================================================================
        // Création de la première demande avec $user 1
        //=========================================================================

        $now = new DateTimeImmutable();
        $jour1 = new Day(new DateTimeImmutable('17-09-2018'));
        $jour2 = new Day(new DateTimeImmutable('18-09-2018'));
        $jour3 = new Day(new DateTimeImmutable('19-09-2018'));
        $jour4 = new Day(new DateTimeImmutable('20-09-2018'));
        $joursA = [$jour1, $jour2, $jour3, $jour4 ];
        $demand1 = new HolidayDemand($user1, $joursA, '', $now);


        //=========================================================================
        // Création de la deuxième demande avec $user 2
        //=========================================================================

        $jour5 = new Day(new DateTimeImmutable('14-10-2018'));
        $jour6 = new Day(new DateTimeImmutable('15-10-2018'));
        $jour7 = new Day(new DateTimeImmutable('16-10-2018'));
        $jour8 = new Day(new DateTimeImmutable('17-10-2018'));
        $joursB = [$jour5, $jour6, $jour7, $jour8 ];
        $EmiseenSeptembre = new DateTimeImmutable('2018-09-02');
        $demand2 = new HolidayDemand($user2, $joursB, '', $EmiseenSeptembre);


        //=========================================================================
        // Création de la deuxième demande avec $user 3
        //=========================================================================

        $jour9 = new Day(new DateTimeImmutable('15-09-2018'));
        $jour10 = new Day(new DateTimeImmutable('16-09-2018'));
        $joursC = [$jour9, $jour10];
        $EmiseenAout = new DateTimeImmutable('2018-08-25');
        $demand3 = new HolidayDemand($user3, $joursC, '', $EmiseenAout);


        //=========================================================================
        // Persistence des données
        //=========================================================================

//        $manager->persist($jour1);
//        $manager->persist($jour2);
//        $manager->persist($jour3);
//        $manager->persist($jour4);

        $manager->persist($demand1);
        $manager->persist($demand2);
        $manager->persist($demand3);

        $manager->flush();
    }
}

