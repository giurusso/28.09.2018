<?php


namespace App\DTO;


use DateTimeImmutable;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Validator\Constraints\Date;


class DTODays
{
    /**
     * @var \DateTimeImmutable
     */
    private $date;

    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(DateTimeImmutable $date): void
    {
        $this->date = $date;
    }

    /**
     * @var bool
     */
    private $isBlocked;


    public function __construct()
    {
        $this->date = new DateTimeImmutable();
    }
}
