<?php

namespace App\DTO;

use DateTimeImmutable;
use DateTimeInterface;

class DTOHolidayDemand
{

    /**
     * @var DateTimeImmutable
     */
    private $startDate;


    /**
     * @return DateTimeImmutable
     */
    public function getStartDate(): ?DateTimeImmutable
    {
        return $this->startDate;
    }


    /**
     * @param DateTimeImmutable $date
     */
    public function setStartDate(DateTimeImmutable $date): void
    {
        $this->startDate = $date;
    }


    /**
     * @var DateTimeImmutable
     */
    private $endDate;


    /**
     * @return DateTimeImmutable
     */
    public function getEndDate(): ?DateTimeImmutable
    {
        return $this->endDate;
    }


    /**
     * @param DateTimeImmutable $date
     */
    public function setEndDate(DateTimeImmutable $date): void
    {
        $this->endDate = $date;
    }

    /**
     * @var string
     */
    private $comment;

    /**
     * @return null|string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $text
     */
    public function setComment(string $text) : void
    {
        $this->comment = $text;
    }


    /**
     * @var int
     */
    private $holidayModality;


    /**
     * @return int
     */
    public function getHolidayModality(): int
    {
        return $this->holidayModality;
    }


    /**
     * @param int $holidayModality
     */
    public function setHolidayModality(int $holidayModality): void
    {
        $this->holidayModality = $holidayModality;
    }


    public function __construct()
    {
        $this->startDate = new DateTimeImmutable();
        $this->endDate = new DateTimeImmutable();

    }
}
