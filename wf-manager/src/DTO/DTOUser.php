<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 10.09.2018
 * Time: 23:09
 */



namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


class DTOUser
{

    //=========================================================================
    // Properties
    //=========================================================================

    /**
     * @var string
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     *
     * @Assert\Regex(
     *     pattern="/@swiss\.com$/",
     *     message="L'adresse email doit être de type @swiss.com"
     * )
     */
    private $email;

    /**
     * @return string
     */
    public function getEmail() : ?string
    {
        return $this->email;
    }

    public function setEmail($email) : void
    {
        $this->email = $email;
    }

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 8,
     *      max = 50,
     *      minMessage = "Le mot de passe doit contenir au moins 8 caractères",
     *      maxMessage = "Le mot de passe doit ne peut pas dépasser 50 caractères",
     * )
     */
    private $password;

    /**
     * @return string
     */
    public function getPassword() : ?string
    {
        return $this->password;
    }

    public function setPassword($password) : void
    {
//        $this->password = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
        $this->password = $password;
    }

    /**
     * @var string
     */
    private $lastName;

    /**
     * @return string
     */
    public function getLastName() : ?string
    {
        return $this->lastName;
    }

    public function setLastName($lastName) : void
    {
        $this->lastName = $lastName;
    }

    /**
     * @var string
     */
    private $firstName;

    /**
     * @return string
     */
    public function getFirstName() : ?string
    {
        return $this->firstName;
    }

    public function setFirstName($firstName) : void
    {
        $this->firstName = $firstName;
    }

    /**
     * @var string
     */
    private $role;

    public function getRole() : ?string
    {
        return $this->role;
    }

    public function setRole($role) : void
    {
        $this->role = $role;

    }

}
