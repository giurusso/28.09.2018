<?php

namespace App\Entity;

use App\DTO\DTODays;
use App\Repository\HolidayDemandRepository;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use LogicException;
use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Validator\Constraints\Date;


/**
 * @ORM\Entity(repositoryClass="App\Repository\DayRepository")
 * @Table(name="days",uniqueConstraints={@UniqueConstraint(name="idx_date", columns={"date"})})
 */
class Day
{

    //=========================================================================
    // Constants
    //=========================================================================

    public const VACATION = 1;
    public const REST = 2;
    public const HOURLY_BALANCE = 3;
    public const UNPAID = 4;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @return int
     */
    public function getId() : Integer
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="date_immutable")
     * @OrderBy({"name" = "ASC"})
     */
    private $date;

    /**
     * @return \DateTimeImmutable
     */
    public function getDate() : DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBlocked;

    public function getIsBlocked() : bool
    {
        return $this->isBlocked;
    }

    public function block() : void
    {
        $this->isBlocked = true;
    }

    public function unBlock() : void
    {
        $this->isBlocked = false;
    }


    //=========================================================================
    // Constructor
    //=========================================================================

    public function __construct(DateTimeImmutable $date)
    {
        $this->date = $date;
        $this->isBlocked = false;
    }
}
