<?php declare(strict_types=1); // permet d'être averti en cas d'erreur de type

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use LogicException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User
{

    //=========================================================================
    // Constants
    //=========================================================================


    //=========================================================================
    // Properties
    //=========================================================================

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId() : int
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    public function getEmail() : string
    {
        return $this->email;
    }


    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 8,
     *      minMessage = "Le mot de passe doit contenir au moins 8 caractères",
     *      maxMessage = "Le mot de passe doit ne peut pas dépasser 50 caractères",
     * )
     */
    private $password;

    public function getPassword() : string
    {
        return $this->password;
    }

    public function changePassword(string $newPassword) : void
    {
        $this->password = password_hash($newPassword, PASSWORD_BCRYPT, ['cost' => 12]);
    }


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    public function getLastName() : string
    {
        return $this->lastName;
    }


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    public function getFirstName() : string
    {
        return $this->firstName;
    }

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $role;

    public function getRole() : string
    {
        return $this->role;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $responsible;

    public function getResponsible() : ?User
    {
        return $this->responsible;
    }

    public function promoteToEmployee(){
        if ($this->role==='ROLE_EMPLOYEE'){
            throw new LogicException('The user role is already ROLE_EMPLOYEE');
        }
        $this->role = 'ROLE_EMPLOYEE';
    }

    public function promoteToManager(){
        if($this->role==='ROLE_MANAGER'){
            throw new LogicException('The user role is already ROLE_MANAGER');
        }
        $this->role = 'ROLE_MANAGER';
    }

    public function promoteToSupervisor(){
        if($this->role==='ROLE_SUPERVISOR'){
            throw new LogicException('The user role is already ROLE_SUPERVISOR');
        }
        $this->role = 'ROLE_SUPERVISOR';
    }

    public function promoteToDirector(){
        if($this->role==='ROLE_DIRECTOR'){
            throw new LogicException('The user role is already ROLE_DIRECTOR');
        }
        $this->role = 'ROLE_DIRECTOR';
    }

    public function promoteToAdministrator(){
        if($this->role==='ROLE_ADMINISTRATOR'){
            throw new LogicException('The user role is already ROLE_ADMINISTRATOR');
        }
        $this->role = 'ROLE_ADMINISTRATOR';
    }

    public function userDeactivate(){
        if ($this->role === 'ROLE_USER'){
            throw new LogicException('Cannot be downgraded because the user role is already ROLE_DISABLED');
        }
        if ($this->role === 'ROLE_ADMINISTRATOR'){
            throw new LogicException('Cannot deactivate an ADMINISTRATOR user.');
        }
        $this->role = 'ROLE_USER';
    }

    public function userManagerDefine($promoterUser){
        if($this->role==='ROLE_USER' || $this->role==='ROLE_SUPERVISOR' || $this->role==='ROLE_DIRECTOR' || $this->role==='ROLE_ADMINISTRATOR'){
            throw new LogicException('User role is such that it cannot be included into any team');
        }
        $this->responsible = $promoterUser;
    }


//    /**
//     * @ORM\JoinColumn(nullable=false)
//     */
//    private $userGroup;
//
//    public function getUserGroup() : UserTeam
//    {
//        return $this->userGroup;
//    }



    //=========================================================================
    // Constructor
    //=========================================================================

    public function __construct(string $email, string $password, string $lastName, string $firstName)
    {
        $this->email = $email;
        $this->changePassword($password);
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->role = 'ROLE_USER';
    }

}
