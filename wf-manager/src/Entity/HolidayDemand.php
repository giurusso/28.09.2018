<?php declare(strict_types=1); // permet d'être averti en cas d'erreur de type

namespace App\Entity;

use App\DataFixtures\DemandsFixtures;
use App\DTO\DTOHolidayDemand;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OrderBy;
use LogicException;
use DateTimeImmutable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HolidayDemandRepository")
 * @ORM\Table(name="holidaydemands")
 */
class HolidayDemand
{

    //=========================================================================
    // Constants
    //=========================================================================

    public const STATUS_PENDING = 1;
    public const STATUS_ACCEPTED = 2;
    public const STATUS_REFUSED = 3;
    public const STATUS_UNPROCESSED = 4;
    public const STATUS_RESERVED = 5;


    //=========================================================================
    // Properties
    //=========================================================================

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId() : int
    {
        return $this->id;
    }


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $employee;

    public function getEmployee(): User
    {
        return $this->employee;
    }


    /**
     * @ManyToMany(targetEntity="App\Entity\Event", cascade={"persist"})
     * @JoinTable(name="holidaydemands_to_events",
     *      joinColumns={@JoinColumn(name="holidaydemand_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="event_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $events;

    public function getEvents() : Collection
    {
        return $this->events;
    }


    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private $createdDate;

    public function getCreatedDate(): DateTimeInterface
    {
        return $this->createdDate;
    }


    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private $startDate;

    public function getStartDate() : DateTimeImmutable
    {
        return $this->startDate;
    }



    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private $endDate;

    public function getEndDate() : DateTimeImmutable
    {
        return $this->endDate;
    }


    /**
     * @ORM\Column(type="smallint", nullable=false)
     * @OrderBy({"status" = "ASC"})
     */
    private $status;

    public function getStatus() : int
    {
        return $this->status;
    }

    public function reserve(User $manager, string $comment) {
        if($comment==null){
            $comment = '';
        }

        $this->status = self::STATUS_RESERVED;

        $this->events[] = new Event(Event::HOLIDAY_DEMAND_RESERVED, $manager, $comment);
    }

    public function getStatusString() : string
    {
        return 'holiday.demand.status.'.$this->status;
    }

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="App\Entity\Day", cascade={"persist"})
     * @ORM\JoinTable(name="holidaydemands_to_days")
     * @OrderBy({"date" = "ASC"})
     */
    private $days;

    //persistent collection = provient de la BD
    //array collection = provient de nulle part


    /**
     * @return Day[]|Collection
     */
    public function getDays():Collection
    {
        return $this->days;
    }


    private function calculateStartDate() : DateTimeImmutable
    {
        $startDate = null;
        foreach($this->getDays() as $day){
            $date = $day->getDate();
            if ($startDate == null || $date < $startDate){
                $startDate = $date;
            }
        }

        if ($startDate == null){
            throw new LogicException('HolidayDemand should have at least one day');
        }

        return $startDate;
    }


    private function calculateEndDate() : DateTimeImmutable
    {
        $endDate = null;
        foreach($this->getDays() as $day){
            $date = $day->getDate();
            if ($endDate == null || $date > $endDate){
                $endDate = $date;
            }
        }

        if ($endDate == null){
            throw new LogicException('HolidayDemand should have at least one day');
        }

        return $endDate;
    }


    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $holidayModality;


    public function getHolidayModality() : int
    {
        return $this->holidayModality;
    }


    /**
     * @return string
     */
    public function getHolidayModalityString(): string
    {
//        return $this->holidayModality;
        return 'holiday.demand.modality.'.$this->holidayModality;
    }


    //=========================================================================
    // Constructor
    //=========================================================================

    /**
     * Holiday constructor.
     * @param User $employee
     * @param Day[] $days
     * @param string $comment
     * @param DateTimeInterface|null $creationDate
     * @throws \Exception
     */
    public function __construct(User $employee, array $days, string $comment, ?DateTimeInterface $creationDate = null)
    {
        $this->createdDate = $creationDate ?? new DateTimeImmutable();
        $this->employee = $employee;
        $this->status = 1;
        $this->days = new ArrayCollection();
        foreach ($days as $day){
            $this->days->add($day);
        }
        $this->startDate = $this->calculateStartDate();
        $this->endDate = $this->calculateEndDate();

        $this->events[] = new Event(Event::HOLIDAY_DEMAND_CREATED, $employee, $comment);
    }


    //=========================================================================
    // Public methods
    //=========================================================================

    public function accept(User $manager, string $comment) : void
    {
        if ($this->getStatus() !== self::STATUS_PENDING && $this->getStatus() !== self::STATUS_RESERVED) {
            throw new LogicException("Cannot accept because of invalid Holiday status (".$this->status.")");

        }

        $this->events[] = new Event(Event::HOLIDAY_DEMAND_ACCEPTED, $manager, $comment);

        $this->status = self::STATUS_ACCEPTED;
    }


    public function refuse(User $manager, string $comment) : void
    {
        if($this->status !== self::STATUS_PENDING){
            throw new LogicException("Cannot refuse beacause of invalid Holiday status (".$this->status.")");
        }

        $this->events[] = new Event(Event::HOLIDAY_DEMAND_REFUSED, $manager, $comment);

        $this->status = self::STATUS_REFUSED;
    }

    public function outdatedStatusSetting():void
    {
        if($this->status !== self::STATUS_PENDING){
            throw new LogicException("Cannot register as outdated beacause of invalid Holiday status (".$this->status.")");
        }

        $this->status = self::STATUS_UNPROCESSED;
    }



    //=========================================================================
    // ControllerHelpers
    //=========================================================================


}
