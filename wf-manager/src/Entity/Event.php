<?php declare(strict_types=1); // permet d'être averti en cas d'erreur de type


namespace App\Entity;


use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity()
 * @Table(name="events")
 */
class Event
{

    //=========================================================================
    // Constants
    //=========================================================================

    public const HOLIDAY_DEMAND_CREATED = 1;
    public const HOLIDAY_DEMAND_ACCEPTED = 2;
    public const HOLIDAY_DEMAND_REFUSED = 3;
    public const HOLIDAY_DEMAND_RESERVED = 4;


    //=========================================================================
    // Properties
    //=========================================================================

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private  $id;

    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;

    public function getDate() : DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;


    public function getUser() : User
    {
        return $this->user;
    }

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    public function getType() : int
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTypeString(): string
    {
//        return $this->holidayModality;
        return 'event.type.'.$this->type;
    }


    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $comment;

    /**
     * @return string
     */
    public function getComment() : string
    {
        return $this->comment;
    }


    /**
     * Event constructor.
     * @param int $type
     * @param User $user
     * @param string $comment
     * @throws \Exception
     */
    public function __construct(int $type, User $user, string $comment=''){
        $this->date = new DateTimeImmutable();
        $this->type = $type;
        $this->user = $user;
        $this->comment = $comment;
    }

}
