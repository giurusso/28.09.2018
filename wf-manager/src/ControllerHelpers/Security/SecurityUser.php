<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 02.09.2018
 * Time: 19:08
 */

namespace App\ControllerHelpers\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

class SecurityUser implements UserInterface, EquatableInterface
{

    //========================================================================================================
    // Properties
    //========================================================================================================

    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $userId;


    //========================================================================================================
    // Constructor
    //========================================================================================================

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->userId = $user->getId();
    }


    //========================================================================================================
    // Getters
    //========================================================================================================

    /**
 * Returns the user's uid as a int.
 */
    public function getIdentifier() : int
    {
        return $this->userId;
    }

    /**
     * Returns the user's account.
     */
    public function getUser() : User
    {
        return $this->user;
    }


    //========================================================================================================
    // UserInterface implementation
    //========================================================================================================

    /**
     * Returns the identifier used to authenticate the user.
     */
    public function getUsername() : string
    {
        return $this->user->getEmail();
    }

    /**
     * Returns the encoded password used to authenticate the user.
     */
    public function getPassword() : string
    {
        return $this->user->getPassword();
    }

    /**
     * Returns the salt that was originally used to encode the password.
     */
    public function getSalt() :?string
    {
        // Password not encoded using a salt.
        return null;
    }

    /**
     * Returns the roles granted to the user.
     *
     * @return string[] The user roles
     */
    public function getRoles() : array
    {
        //crée un tableau une seule donnée (en l'occurrence un string), voir classe User
        return [$this->user->getRole()];
    }

    /**
     * Removes sensitive data from the user (eg. plain-text password).
     */
    public function eraseCredentials() : void
    {
        // Unused, there is nothing to hide.
    }


    //========================================================================================================
    // Serializable implementation
    //========================================================================================================

    /**
     * Returns the string (uid) representation of the user.
     */
    public function serialize() : ?string
    {
        return json_encode($this->userId);
    }

    /**
     * Constructs the user from its string (uid) representation.
     */
    public function unserialize($serialized) : void
    {
        $this->userId = json_decode($serialized);
    }


    //========================================================================================================
    // EquatableInterface implementation
    //========================================================================================================

    /**
     * Returns whether the current user is up-to-date or requires re-authentication.
     * @param UserInterface $user
     * @return bool
     */
    public function isEqualTo(UserInterface $user) : bool
    {
        if (!$user instanceof self) {
            return false;
        }

        if ($this->getIdentifier() !== $user->getIdentifier()) {
            return false;
        }

        return true;
    }

}
