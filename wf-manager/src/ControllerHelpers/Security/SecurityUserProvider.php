<?php declare(strict_types=1);


namespace App\ControllerHelpers\Security;


use App\Entity\User;
use App\Repository\UserRepository;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class SecurityUserProvider implements UserProviderInterface
{


    //=========================================================================
    // Properties
    //=========================================================================

    private $userRepository;


    //=========================================================================
    // Constructor
    //=========================================================================


    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }




    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        return $this->fetchUser($username);
    }


    /**
     * Refreshes the user.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * ça vérifie que le "user" passé en argument est bien du même type que le SecurityUser
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException  if the user is not supported
     * @throws UsernameNotFoundException if the user is not found
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof SecurityUser){
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        $username = $user->getUsername();

        return $this->fetchUser($username);
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return SecurityUser::class === $class;
    }

    /**
     * @param string $email
     * @return SecurityUser
     * @var UserRepository
     * @var User
     */
    private function fetchUser (string $email)
    {
        $user = $this->userRepository->findOneBy(['email' => $email]);

        if ($user) {

            return new SecurityUser($user);
        }

        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $email )
        );
    }
}
