<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 28.08.2018
 * Time: 18:09
 */


namespace App\ControllerHelpers;


use App\Entity\Day;
use App\Entity\HolidayDemand;
use App\Repository\HolidayDemandRepository;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use LogicException;

//  application du pattern decorator ici


class DemandData
{

    /**
     * @var HolidayDemand
     */
    private $demand;

    /**
     * @return HolidayDemand[]
     */
    public function getDemand():HolidayDemand
    {
        return $this->demand;
    }

    private $demandStatus;

    public function __construct(HolidayDemand $holidayDemand)
    {
        $this->demand = $holidayDemand;
    }

    public function isImminent():bool
    {
        $today = (new DateTimeImmutable())->setTime(0,0,0,0);
        $startDate = $this->demand->getStartDate();

        $isNotOudated = !$this->isOutdated();
        $isWithinFewDays = $startDate <= $today->modify('+ 4 days');

        $isPending = $this->demand->getStatus() === HolidayDemand::STATUS_PENDING;

        return $isNotOudated && $isWithinFewDays && $isPending;
    }

    public function isOutdated():bool
    {
        $today = (new DateTimeImmutable())->setTime(0,0,0,0);
        $startDate = $this->demand->getStartDate();

        $isPending = $this->demand->getStatus() === HolidayDemand::STATUS_PENDING;

        return $startDate < $today && $isPending;
    }

    public function isOld():bool
    {
        $today = (new DateTimeImmutable())->setTime(0,0,0,0);
        $creationDate = $this->demand->getCreatedDate();
        $old = $creationDate <= $today->modify('- 5 days');
        return $old;
    }

    public function getStatus():int
    {
        return $this->demand->getStatus();
    }

    public function getId():int
    {
        return $this->demand->getId();
    }

    public function getCreatedDate():DateTimeInterface
    {
        return $this->demand->getCreatedDate();
    }

    public function canBeAcceptedOrRefused():bool
    {
        $isNotOudated = !$this->isOutdated();
        $isNotProcessed = $this->demand->getStatus() != HolidayDemand::STATUS_ACCEPTED && $this->demand->getStatus() != HolidayDemand::STATUS_REFUSED;

        return $isNotOudated && $isNotProcessed;
    }
}
