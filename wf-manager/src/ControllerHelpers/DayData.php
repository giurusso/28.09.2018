<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 28.08.2018
 * Time: 21:11
 */

namespace App\ControllerHelpers;


use App\Controller\Holiday\DemandShow;
use App\Entity\Day;
use App\Entity\HolidayDemand;
use DateTimeImmutable;
use phpDocumentor\Reflection\Types\Integer;

class DayData
{

    /**
     * @var DateTimeImmutable
     */
    private $date;

    public function getDate() : DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @var int
     */
    private $pendingCount;

    public function getPendingCount() : int
    {
        return $this->pendingCount;
    }

    /**
     * @var int
     */
    private $acceptedCount;

    public function getAcceptedCount() : int
    {
        return $this->acceptedCount;
    }

    /**
     * @var int
     */
    private $refusedCount;

    public function getRefusedCount() : int
    {
        return $this->refusedCount;
    }

    /**
     * @var int
     */
    private $reservedCount;

    public function getReservedCount() : int
    {
        return $this->reservedCount;
    }

    /**
     * @var HolidayDemand[];
     */
    private $demands;

    /**
     * @return HolidayDemand[];
     */
    public function getDemands() : array
    {
        return $this->demands;
    }

    /**
     * @var Day
     */
    private $day;

    public function isBlocked() : bool
    {
        if ($this->day !== null) {
            return $this->day->getIsBlocked();
        }

        return false;
    }

    /**
     * DayData constructor.
     * @param DateTimeImmutable $date
     * @param HolidayDemand[] $demands
     * @param Day $day
     * @throws \Exception
     */
    public function __construct(DateTimeImmutable $date, array $demands, ?Day $day)
    {
        $this->pendingCount = 0;
        $this->acceptedCount = 0;
        $this->refusedCount = 0;
        $this->reservedCount = 0;

        $today = (new DateTimeImmutable())->setTime(0,0,0,0);

        foreach($demands as $demand){
            $isOutdated = $demand->getStartDate() < $today;

            if ($demand->getStatus() === HolidayDemand::STATUS_PENDING && !$isOutdated){
                $this->pendingCount++;
            }else if($demand->getStatus() === HolidayDemand::STATUS_ACCEPTED){
                $this->acceptedCount++;
            }else if($demand->getStatus() === HolidayDemand::STATUS_REFUSED){
                $this->refusedCount++;
            }else if($demand->getStatus() === HolidayDemand::STATUS_RESERVED){
                $this->reservedCount++;
            }
        }
        $this->date = $date;
        $this->demands = $demands;

        $this->day = $day;
    }

}
