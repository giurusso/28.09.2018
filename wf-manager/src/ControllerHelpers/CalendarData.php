<?php


namespace App\ControllerHelpers;


use App\Entity\Day;
use App\Entity\HolidayDemand;
use App\Repository\HolidayDemandRepository;
use DateTimeImmutable;

class CalendarData
{
    public $days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

    public $months = [
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Décembre'
    ];

    private $month;

    public function getMonth(): int
    {
        return $this->month;
    }

    private $year;

    public function getYear(): int
    {
        return $this->year;
    }

    private $dayDataList;

    /**
     * @return DayData[]
     */
    public function getDayDataList(): array
    {

        return $this->dayDataList;
    }

    /**
     * @var
     */
    private $isEmployee;

    public function getIsEmployee(): bool
    {
        return $this->isEmployee;
    }

    /**
     * Month constructor.
     * Month is between 1 and 12
     * @param int|null $year
     * @param int|null $month
     * @param HolidayDemand[] $userallDemands
     * @param bool $isEmployee
     * @param Day[] $allDays
     */
    public function __construct(?int $year = null, ?int $month = null, array $userallDemands, bool $isEmployee, array $allDays)
    {
        if ($year === null) {
            $year = intval(date('Y'));
        }
        if ($month === null || $month < 1 || $month > 12) {
            $month = intval(date('m'));
        }

        $this->year = $year;
        $this->month = $month;
        $demandListByDays = $this->createDemandListByDay($userallDemands);
        $this->dayDataList = $this->createDayDataList($demandListByDays, $year, $month, $allDays);
        $this->isEmployee = $isEmployee;
    }

    /**
     * returns the first day of the month
     * @return \DateTimeImmutable
     * @throws \Exception
     */
    public function getStartingDay(): DateTimeImmutable
    {
//        return new \DateTimeImmutable ("{$this->year}-{$this->month}-01");
//        return new \DateTime::createFromFormat('Y-m-d', $this->year . '-' . $this->month . '-01');
        return (new DateTimeImmutable())->setDate($this->year, $this->month, 1);
    }


    /**
     * returns the month name
     * @return string
     */
    public function toString(): string
    {
        return $this->months[$this->month - 1] . ' ' . $this->year;
    }

    /**
     * Renvoie le nombre de semaines dans le mois
     * @return int
     */
    public function getWeeks(): int
    {
        $start = $this->getStartingDay();
        $end = $start->modify('+1 month -1 day');
        $startWeek = intval($start->format('W'));
        $endWeek = intval($end->format('W'));
        if ($endWeek === 1) {
            $endWeek = intval($end->modify('- 7 days')->format('W')) + 1;
        }
        $weeks = $endWeek - $startWeek + 1;
        if ($weeks < 0) {
            $weeks = intval($end->format('W'));
        }
//        $weeks = 6;
        return $weeks;
    }

    /**
     * Est-ce que le jour est dans le mois en cours
     * @param \DateTimeImmutable $date
     * @return bool
     * @throws \Exception
     */
    public function withinMonth(\DateTimeImmutable $date): bool
    {
        return $this->getStartingDay()->format('Y-m') === $date->format('Y-m');
    }

    /**
     * Renvoie le mois suivant
     * @return CalendarData
     */
    public function nextMonth(): CalendarData
    {
        $month = $this->month + 1;
        $year = $this->year;
        if ($month > 12) {
            $month = 1;
            $year += 1;
        }
        return new CalendarData($year, $month, [], $this->isEmployee, []);
    }


    /**
     * Renvoie le mois précédent
     * @return CalendarData
     */
    public function previousMonth(): CalendarData
    {
        $month = $this->month - 1;
        $year = $this->year;
        if ($month < 1) {
            $month = 12;
            $year -= 1;
        }
        return new CalendarData($year, $month, [], $this->isEmployee, []);
    }


    /**
     * @param HolidayDemand[] $userallDemands
     * @return array
     */
    private function createDemandListByDay(array $userallDemands): array
    {
        $demandListByDays = [];
        foreach ($userallDemands as $demand) {
            foreach ($demand->getDays() as $day) {
                $date = $day->getDate()->format('Y-m-d');
                $demandListByDays[$date][] = $demand;
            }
        }
        return $demandListByDays;
    }


    /**
     * @param Day[] $allDays
     * @param DateTimeImmutable $date
     * @return Day
     */
    private function getDayFromDate(array $allDays, $date) : ?Day
    {
        foreach($allDays as $day){
            if ($day->getDate()->format('Y-m-d') === $date->format('Y-m-d')){
                return $day;
            }
        }
        return null;
    }

    private function createDayDataList(array $demandListByDays, int $year, int $month, array $allDays): array
    {
//        dump($allDays);
        $dayDataList = [];
        $startOfMonth = (new DateTimeImmutable())->setDate($year, $month, 1)->setTime(0, 0, 0, 0);
        $endOfMonth = $startOfMonth->modify('+1 month')->modify('-1 days');
        while ($startOfMonth <= $endOfMonth) {
            $formattedDate = $startOfMonth->format('Y-m-d');

            $existingDay = $this->getDayFromDate($allDays, $startOfMonth);
//            dump($existingDay);


            if (isset($demandListByDays[$formattedDate])) {
                $dayDataList[] = new DayData($startOfMonth, $demandListByDays[$formattedDate], $existingDay);
            } else {
                $dayDataList[] = new DayData($startOfMonth, [], $existingDay);
            }
            $startOfMonth = $startOfMonth->modify('+1 days');
        }
        return $dayDataList;
    }
}
