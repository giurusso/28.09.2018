<?php

namespace App\Repository;

use App\Entity\HolidayDemand;
use App\Entity\User;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Validator\Constraints\Date;
use Doctrine\DBAL\Types\Type;

/**
 * @method HolidayDemand|null find($id, $lockMode = null, $lockVersion = null)
 * @method HolidayDemand|null findOneBy(array $criteria, array $orderBy = null)
 * @method HolidayDemand[]    findAll()
 * @method HolidayDemand[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HolidayDemandRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HolidayDemand::class);
    }

//    public function findAll()
//    {
//        return $this->createQueryBuilder('hd')
//            ->orderBy('hd.day');
//    }



    public function findByUser(User $user): array
    {
        $qb = $this->createQueryBuilder('hd')
            ->addSelect(['e', 'd'])
            ->innerJoin('hd.employee', 'e')
            ->leftJoin('hd.days', 'd') //leftJoin : récupère les demandes mêmes sans jours associés
            ->andWhere('e.id = :userId')
            ->orderBy('hd.startDate', 'DESC')
            ->setParameter('userId', $user->getId())
            ;

        return $qb->getQuery()->getResult();
    }


    public function demandsPerDay(DateTimeInterface $date){
        return $this->createQueryBuilder('hd')
            ->select('hd')
            ->innerJoin('hd.days', 'd', 'WITH', 'd.date = :date')
            ->setParameter('date', $date, Type::DATE )
            ->orderBy('hd.status')
            ->getQuery()
            ->getResult();
    }

    public function demandsPerDayWithoutUnprocessedDemands(DateTimeInterface $date, DateTimeInterface $today){
        return $this->createQueryBuilder('hd')
            ->select('hd')
            ->innerJoin('hd.days', 'd', 'WITH', 'd.date = :date')
            ->where ('hd.startDate > :today')
            ->setParameter('date', $date, Type::DATE)
            ->setParameter('today', $today, Type::DATE)
            ->orderBy('hd.status')
            ->getQuery()
            ->getResult();
    }

//    a été enlevé : d.holidayDemands = hd.id AND

//`$builder = $this->createQueryBuilder('h')
//          ->select('h')
//          ->innerJoin('h.days', 'd', 'WITH', 'd.demand = h.id AND d.date = :date'')
//          ->setParameter('date', $date)
//          ->getQuery();

//$builder = $this->em->getRepository(HolidayDemand::class)->createQueryBuilder('h')
//    public function createQueryBuilder(?string $alias = null, ?string $indexBy = null): QueryBuilder

//return $this->em->getRepository(HolidayDemand::class)->createQueryBuilder('d')
//    public function createQueryBuilder(?string $alias = null, ?string $indexBy = null): QueryBuilder



//    /**
//     * @return Holiday[] Returns an array of Holiday objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Holiday
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
