<?php
/**
 * Created by PhpStorm.
 * User: pro
 * Date: 16.08.18
 * Time: 13:34
 */

namespace App\Controller\Home;


use App\ControllerHelpers\Security\SecurityUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class Home extends AbstractController
{

    /**
     * @Route("/manager", name="manager_home")
     * @param AuthorizationCheckerInterface $auth
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function home(AuthorizationCheckerInterface $auth)
    {
        if ($auth->isGranted('ROLE_EMPLOYEE') || !$auth->isGranted('ROLE_USER')) {
            return $this->render('Home/Home.twig', [

            ]);
        } else {
            return $this->render('Home/HomeUser.twig', [

            ]);
        }
    }
}
