<?php

namespace App\Controller\Team;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserPromoteToEmployee extends AbstractController
{

    /**
     * @param EntityManagerInterface $entityManager
     * @param int $userId
     * @param AuthorizationCheckerInterface $authChecker
     * @return Response
     * @Security("is_granted('ROLE_MANAGER') or is_granted('ROLE_DIRECTOR') or is_granted('ROLE_ADMINISTRATOR')")
     * @Route("/manager/user/promoteToEmployee/{userId}", name="manager_user_promote_to_employee")
     */
    public function __invoke(EntityManagerInterface $entityManager, int $userId, AuthorizationCheckerInterface $authChecker): Response
    {
        $securityUser = $this->getUser();
        $promoterUser = $securityUser->getUser();
        $applicantUser = $entityManager->getRepository(User::class)->find($userId);

        if (!$authChecker->isGranted('ROLE_SUPERVISOR') && !$authChecker->isGranted('ROLE_MANAGER') && !$authChecker->isGranted('ROLE_DIRECTOR') && $promoterUser->getRole() !== 'ROLE_ADMINISTRATOR'){
            throw new AccessDeniedException('Logged user is not allowed to promote this applicant user');
        }

        $applicantUser->promoteToEmployee();

        $entityManager->flush();

        return $this->redirectToRoute('manager_team_users_show', ['id' => $applicantUser->getId()]);
    }
}
