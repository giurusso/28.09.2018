<?php


namespace App\Controller\Team;

use App\DTO\DTOUser;
use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserConsulting extends AbstractController
{
    /**
     * @Route("/manager/users/{id}", name="manager_team_users_show", requirements={"id" = "\d+"})
     * @param int $id
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param UserRepository $userRepository
     * @param AuthorizationCheckerInterface $authChecker
     * @return Response
     * @Security("is_granted('ROLE_EMPLOYEE') or is_granted('ROLE_MANAGER') or is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_DIRECTOR')")
     */
    public function userShow(int $id, Request $request, FormFactoryInterface $formFactory, UserRepository $userRepository, AuthorizationCheckerInterface $authChecker)
    {
        $form = $formFactory->create(ChangePasswordType::class, new DTOUser());
        $form->handleRequest($request);

        $securityUser = $this->getUser();
        $managerUser = $securityUser->getUser();
        $applicantUser = $userRepository->find($id);

        if($form->isSubmitted()) {
            $data = $form->getData();
            dump($data);
            if (!$authChecker->isGranted('ROLE_ADMINISTRATOR') && $managerUser !== $applicantUser){
                throw new AccessDeniedException('Logged user is not allowed to change this password');
            }


            if ($form->isValid()) {
                $newPassword = $data->getPassword();
                $applicantUser->changePassword($newPassword);
                $this->getDoctrine()->getManager()->persist($applicantUser);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash("success", "Le mot de passe bien été modifié");
                return $this->redirectToRoute('manager_team_users_show', ['id' => $id]);

            }
        }

        return $this->render('/Team/UserConsulting.twig',[
            'User' => $applicantUser,
            'formular' => $form->createView(),


        ]);
    }
}
