<?php

namespace App\Controller\Team;


use App\DTO\DTOUser;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserCreate extends AbstractController
{
    /**
     * @Route("/manager/users/create", name="manager_user_create")
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param UserRepository $userRepository
     * @return Response
     */
    public function __invoke(Request $request, FormFactoryInterface $formFactory, UserRepository $userRepository): Response
    {
        $form = $formFactory->create(UserType::class, new DTOUser());

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $data = $form->getData();

            $existingUser = $userRepository->findOneBy(['email' => $data->getEmail()]);

            if($existingUser instanceof User){
                $form->addError(new FormError('Courriel déjà utilisé'));
            }

            if ($form->isValid()) {

                $email = $data->getEmail();

                $password = $data->getPassword();

                $lastName = $data->getLastName();

                $firstName = $data->getFirstName();

                $user = new User($email, $password, $lastName, $firstName);

                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();
                return $this->redirectToRoute('login');
            }
        }

        return $this->render('/Team/UserCreate.twig', [
            'formular' => $form->createView(),
        ]);

    }
}