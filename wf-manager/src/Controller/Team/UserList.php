<?php

namespace App\Controller\Team;


use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserList extends AbstractController
{
    /**
     * @Route("/manager/users", name="manager_team_users")
     * @Security("is_granted('ROLE_MANAGER') or is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_DIRECTOR')")
     */
    public function __invoke(EntityManagerInterface $em, UserRepository $repo)
    {
        if (false){
            $user = new User("neloson", "Nels", "Oleson", true);
            $em->persist($user);

            $em->flush();
        }

        $url = $this->generateUrl('manager_team_users');


        $users = $repo->findBy(
            [],
            ['lastName' => 'ASC']
        );


        return $this->render('/Team/UserList.twig',[
            'Users' => $users,
            'url' => $url,
        ]);
    }


    /**
     * Ne figure ici que provisoirement
     * @Route("/usersAdmin", name="users_admin")
     */
    public function usersAdministration()
    {

        return $this->render('Team/UserAdmin.twig');
    }
}
