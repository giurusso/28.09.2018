<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 16.09.2018
 * Time: 21:42
 */

namespace App\Controller\Team;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserPromoteToDirector extends AbstractController
{

    /**
     * @param EntityManagerInterface $entityManager
     * @param int $userId
     * @param AuthorizationCheckerInterface $authChecker
     * @return Response
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @Route("/manager/user/promoteToDirector/{userId}", name="manager_user_promote_to_director")
     */
    public function __invoke(EntityManagerInterface $entityManager, int $userId, AuthorizationCheckerInterface $authChecker): Response
    {
        $applicantUser = $entityManager->getRepository(User::class)->find($userId);

        if(!$authChecker->isGranted('ROLE_ADMINISTRATOR')){
            throw new AccessDeniedException('Logged user is not allowed to promote this applicant user');
        }

        $applicantUser->promoteToDirector();

        $entityManager->flush();

        return $this->redirectToRoute('manager_team_users_show', ['id' => $applicantUser->getId()]);
    }
}
