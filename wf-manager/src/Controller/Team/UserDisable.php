<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 17.09.2018
 * Time: 18:29
 */

namespace App\Controller\Team;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserDisable extends AbstractController
{

    /**
     * @param EntityManagerInterface $entityManager
     * @param int $userId
     * @return Response
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @Route("/manager/user/userDeactivate/{userId}", name="manager_user_deactivate")
     */
    public function __invoke(EntityManagerInterface $entityManager, int $userId, AuthorizationCheckerInterface $authChecker) : Response
    {
        $securityUser = $this->getUser();
        $applicantUser = $entityManager->getRepository(User::class)->find($userId);

        if(!$authChecker->isGranted('ROLE_ADMINISTRATOR')){
            throw new AccessDeniedException('Logged user is not allowed to disable this applicant user');
        }

        $applicantUser->userDeactivate();

        $entityManager->flush();

        return $this->redirectToRoute('manager_team_users_show', ['id' => $applicantUser->getId()]);
    }
}
