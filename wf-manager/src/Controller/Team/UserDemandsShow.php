<?php

namespace App\Controller\Team;

use App\ControllerHelpers\DemandData;
use App\Entity\HolidayDemand;
use App\Entity\User;
use App\Repository\HolidayDemandRepository;
use App\Repository\UserRepository;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class UserDemandsShow extends AbstractController
{

    /**
     * @Route("/manager/users/{user}/demands/", name="manager_users_holiday_demands", requirements={"id" = "\d+"})
     * @Security("is_granted('ROLE_MANAGER') or is_granted('ROLE_DIRECTOR') or is_granted('ROLE_ADMINISTRATOR')")
     */
    public function __invoke(User $user, HolidayDemandRepository $repo)
    {
        $userallDemands = $repo->findByUser($user);
        $demandData = [];
        foreach ($userallDemands as $d){
            $demandData[] = new DemandData($d);
        }

        return $this->render('Team/UserDemandsShow.twig',[
            'UserallDemands' => $userallDemands,
            'DemandData' => $demandData,
            'User' => $user,
        ]);
    }
}
