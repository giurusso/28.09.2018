<?php


namespace App\Controller\Team;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserManagerDefine extends AbstractController
{

    /**
     * @param EntityManagerInterface $entityManager
     * @param int $userId
     * @param AuthorizationCheckerInterface $authChecker
     * @return Response
     * @Security("is_granted('ROLE_MANAGER')")
     * @Route("/manager/user/defineManager/{userId}", name="manager_user_define_manager")
     */
    public function __invoke(EntityManagerInterface $entityManager, int $userId, AuthorizationCheckerInterface $authChecker): Response
    {
        $securityUser = $this->getUser();
        $managerUser = $securityUser->getUser();
        $applicantUser = $entityManager->getRepository(User::class)->find($userId);

        if (!$authChecker->isGranted('ROLE_MANAGER') && $managerUser !== $applicantUser){
            throw new AccessDeniedException('Logged user is not allowed to integrate this applicant user to his team');
        }

        $applicantUser->userManagerDefine($managerUser);

        $entityManager->flush();

        return $this->redirectToRoute('manager_team_users_show', ['id' => $applicantUser->getId()]);
    }
}
