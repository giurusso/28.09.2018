<?php

namespace App\Controller\Holiday;


use App\Repository\HolidayDemandRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

class DemandListPerDay extends AbstractController
{

    /**
     * @Route("/manager/rh/{dateString}/demands/", name="manager_holiday_date_demands", requirements={"\d{4}\-\d{2}\-\d{2}"})
     * @Security("is_granted('ROLE_MANAGER') or is_granted('ROLE_DIRECTOR') or is_granted('ROLE_ADMINISTRATOR')")
     */
    public function __invoke(string $dateString, EntityManagerInterface $em, HolidayDemandRepository $repo)
    {
        $date = DateTimeImmutable::createFromFormat('Y-m-d', $dateString);

        $allDemandsPerDay = $repo->demandsPerDay($date);

        return $this->render('/Holiday/DemandListPerDay.twig',[
            "AllDemandsPerDay" => $allDemandsPerDay,
            "Date" => $date,
        ]);
    }
}
