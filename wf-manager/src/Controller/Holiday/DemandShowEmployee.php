<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 09.09.2018
 * Time: 19:39
 */

namespace App\Controller\Holiday;


use App\ControllerHelpers\DemandData;
use App\Entity\HolidayDemand;
use App\Repository\HolidayDemandRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DemandShowEmployee extends AbstractController
{
    /**
     * @Route("/manager/employee/demands/{id}", name="employee_manager_holiday_demands_show", requirements={"id" = "\d+"})
     * @Security("is_granted('ROLE_EMPLOYEE')")
     */
    public function __invoke(HolidayDemand $demand, HolidayDemandRepository $repo)
    {
        $demandData = new DemandData($demand);

        $isEmployee = true;

        return $this->render('/Holiday/DemandShowEmployee.twig', [
            'Demand' => $demand,
            'DemandData' => $demandData,
            'IsEmployee' => $isEmployee,
        ]);
    }
}
