<?php

namespace App\Controller\Holiday;

use App\ControllerHelpers\DemandData;
use App\Entity\HolidayDemand;
use App\Entity\User;
use App\Repository\ArticleRepository;
use App\Repository\HolidayDemandRepository;
use App\Repository\UserRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DemandListEmployee extends AbstractController
{
    /**
     * @Route("/manager/employee/demands", name="employee_manager_holiday_demands")
     * @param EntityManagerInterface $em
     * @param HolidayDemandRepository $holidayDemandRepository
     * @return Response
     * @Security("is_granted('ROLE_EMPLOYEE')")
     */
    public function __invoke(EntityManagerInterface $em, HolidayDemandRepository $holidayDemandRepository)
    {
        $currentUser = $this->getUser();
        $user = $currentUser->getUser();
        $userDemands = $holidayDemandRepository->findByUser($user);

        $demandData = [];
        foreach ($userDemands as $d){
            $demandData[] = new DemandData($d);
        }
        $isEmployee = true;

        return $this->render('/Holiday/DemandListEmployee.twig',[
            'DemandDataList' => $demandData,
            'IsEmployee' => $isEmployee,
        ]);
    }
}
