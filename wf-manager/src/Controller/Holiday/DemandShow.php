<?php

namespace App\Controller\Holiday;

use App\ControllerHelpers\DemandData;
use App\DTO\DTOHolidayDemand;
use App\Entity\HolidayDemand;
use App\Form\HolidayDemandManagementType;
use App\Repository\HolidayDemandRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class DemandShow extends AbstractController
{

    /**
     * @Route("/manager/demands/{id}", name="manager_holiday_demands_show", requirements={"id" = "\d+"})
     * @Security("is_granted('ROLE_MANAGER') or is_granted('ROLE_DIRECTOR') or is_granted('ROLE_ADMINISTRATOR')")
     */
    public function __invoke(HolidayDemand $demand, HolidayDemandRepository $repo, FormFactoryInterface $formFactory, Request $request, EntityManagerInterface $em, int $id) : Response
    {
        $securityUser = $this->getUser();
        $user = $securityUser->getUser();

        $form = $formFactory->create(HolidayDemandManagementType::class);

        $form->handleRequest($request);

        $demand = $repo->find($id);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $isUserNotAllowed = ($user->getRole() !== 'ROLE_MANAGER' && $user->getRole() !== 'ROLE_SUPERVISOR');

            if($isUserNotAllowed){
                $form->addError(new FormError('Seuls les Teamleaders ou le Supervisor peuvent donner suite à une demande.'));
            }

            if($form->isValid()) {

                if ($data['comment'] == null) {
                    $data['comment'] = '';
                }
                if ($form->get('acceptButton')->isClicked()) {
                    try {
                        $demand->accept($user, $data['comment']);
                    } catch (Exception $exception) {
                        return $this->redirectToRoute('manager_holiday_demands_show', ['id' => $demand->getId()]);
                    }

                } elseif ($form->get('refuseButton')->isClicked()) {
                    try {
                        $demand->refuse($user, $data['comment']);
                    } catch (Exception $exception) {
                        return $this->redirectToRoute('manager_holiday_demands_show', ['id' => $demand->getId()]);
                    }
                }

                $em->persist($demand);
                $em->flush();
                return $this->redirectToRoute('manager_holiday_demands_show', ['id' => $demand->getId()]);
            }
        }

        $isEmployee = false;

        $demandData = new DemandData($demand);

        return $this->render('/Holiday/DemandShow.twig',[
            'Demand' => $demand,
            'DemandData' => $demandData,
            'ValidationForm' => $form->createView(),
            'IsEmployee' => $isEmployee,

        ]);
    }
}
