<?php


namespace App\Controller\Holiday;


use App\Entity\Day;
use App\Repository\DayRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DayBlock extends AbstractController
{

    /**
     * @param string $dateString
     * @param EntityManagerInterface $entityManager
     * @param DayRepository $dayRepository
     * @param int $dayId
     * @param AuthorizationCheckerInterface $authChecker
     * @return Response
     * @Security("is_granted('ROLE_SUPERVISOR')")
     * @Route("/manager/rh/dayblock{dateString}/", name="manager_holiday_dayblock", requirements={"\d{4}\-\d{2}\-\d{2}"})
     */
    public function __invoke(string $dateString, EntityManagerInterface $entityManager, DayRepository $dayRepository, AuthorizationCheckerInterface $authChecker) : Response
    {
        if(!$authChecker->isGranted('ROLE_MANAGER')){
            throw new AccessDeniedException('Logged user is not allowed to block days');
        }

        $dayToBeBlocked = new DateTimeImmutable($dateString);

        $newDay = $dayRepository->findOneBy(['date' => $dayToBeBlocked]);
        if (!$newDay) {
            $newDay = new Day($dayToBeBlocked);
            $newDay->block();
        }else{
            if(!$newDay->getIsBlocked()) {
                $newDay->block();
            }else{
                $newDay->unBlock();
            }
        }

        $this->getDoctrine()->getManager()->persist($newDay);
        $entityManager->flush();

        return $this->redirectToRoute('calendar', ['year' => $dayToBeBlocked->format('Y'), 'month' => $dayToBeBlocked->format('m')]);

    }

}
