<?php

namespace App\Controller\Holiday;

use App\ControllerHelpers\DemandData;
use App\Entity\HolidayDemand;
use App\Entity\User;
use App\Repository\ArticleRepository;
use App\Repository\HolidayDemandRepository;
use App\Repository\UserRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class DemandList extends AbstractController
{
    /**
     * @Route("/manager/rh/demands", name="manager_holiday_demands")
     * @param EntityManagerInterface $em
     * @param HolidayDemandRepository $repo
     * @return Response
     * @Security("is_granted('ROLE_MANAGER') or is_granted('ROLE_DIRECTOR') or is_granted('ROLE_ADMINISTRATOR')")
     * @throws \Exception
     */
    public function __invoke(EntityManagerInterface $em, HolidayDemandRepository $repo)
    {
        $demands = $repo->findBy([],['createdDate' => 'DESC']);

        $demandData = [];
        foreach ($demands as $d){
            $demandData[] = new DemandData($d);
        }
        $isEmployee = false;

        return $this->render('/Holiday/DemandList.twig',[
            'DemandDataList' => $demandData,
            'IsEmployee' => $isEmployee,
        ]);
    }
}
