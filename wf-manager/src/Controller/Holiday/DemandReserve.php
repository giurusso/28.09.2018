<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 06.09.2018
 * Time: 00:15
 */

namespace App\Controller\Holiday;


use App\Entity\HolidayDemand;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DemandReserve extends AbstractController
{

    /**
     * @param EntityManagerInterface $entityManager
     * @param int $demandId
     * @return Response
     * @Security("is_granted('ROLE_EMPLOYEE')")
     * @Route("/manager/demands/reserve/{demandId}", name="manager_demands_reserve")
     */
    public function __invoke (EntityManagerInterface $entityManager, int $demandId) : Response
    {
        $securityUser = $this->getUser();
        $user = $securityUser->getUser();
        $holidayDemand = $entityManager->getRepository(HolidayDemand::class)->find($demandId);
        if($user !== $holidayDemand->getEmployee()) {
            throw new AccessDeniedException('Logged user is not the same as the demand user');
        }

        $holidayDemand->reserve($user, '');
        $entityManager->flush();

        return $this->redirectToRoute('employee_manager_holiday_demands');
    }
}
