<?php

namespace App\Controller\Holiday;

use App\DTO\DTOHolidayDemand;
use App\Entity\Day;
use App\Entity\HolidayDemand;
use App\Entity\User;
use App\Form\HolidayDemandType;
use App\Repository\DayRepository;
use App\Repository\HolidayDemandRepository;
use App\Repository\UserRepository;
use DateInterval;
use DateTimeImmutable;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use App\ControllerHelpers\DemandData;


class DemandCreate extends AbstractController
{

    /**
     * @Route("/manager/users/demands/create", name="manager_holiday_demands_create")
     * @Security("is_granted('ROLE_EMPLOYEE')")
     */
    public function demandCreate(Request $request, FormFactoryInterface $formFactory, DayRepository $dayRepository, HolidayDemandRepository $holidayDemandRepository): Response
    {

        $form = $formFactory->create(HolidayDemandType::class, new DTOHolidayDemand());

        // la variable $securityUser est de type SecurityUser
        $securityUser = $this->getUser();
        $user = $securityUser->getUser();

        $userDemands = $holidayDemandRepository->findByUser($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $formHasErrors = false;

            $data = $form->getData();
            $newDate = $data->getStartDate();
            $endDate = $data->getEndDate();
            $comment = $data->getComment();
            $today = (new DateTimeImmutable())->setTime(0, 0, 0, 0);

            $result = $this->isSameDayAlreadyInAnotherDemandFromSameUser($newDate, $endDate, $userDemands);
            if ($result) {
                $form->addError(new FormError('Cette demande contient une ou plusieurs date(s) déjà contenue(s) dans une autre demande.'));
                $formHasErrors = true;
            }

            if (!$this->isStartDateBeforeEndDate($newDate, $endDate)) {
                $form->addError(new FormError('La date de fin de la période de congé ne peut pas être antérieure à la date de début.'));
                $formHasErrors = true;
            }

            if (!$this->isStartDateTodayOrLaterThanToday($newDate, $today)) {
                $form->addError(new FormError('La demande ne peut porter sur une date située dans le passé.'));
                $formHasErrors = true;
            }

            if ($this->isTooFarInTheFuture($endDate)) {
                $form->addError(new FormError('La planification d\'un congé n\'est possible que deux ans à l\'avance au plus.'));
                $formHasErrors = true;
            }

            if (!$this->isMaxDurationRespected($newDate, $endDate)) {
                $form->addError(new FormError('La demande ne peut porter sur une période de plus de trois mois.'));
                $formHasErrors = true;
            }

            $days = [];

            while ($newDate <= $data->getEndDate()) {
                $newDay = $dayRepository->findOneBy(['date' => $newDate]);
                if (!$newDay) {
                    $newDay = new Day($newDate);
                }

                $blockedDay = ($newDay->getIsBlocked());

                if ($blockedDay) {
                    $form->addError(new FormError('La demande contient un jour bloqué par un Teamleader.'));
                    $formHasErrors = true;
                }
                if ($formHasErrors === true) {
                    break;
                }

                $days[] = $newDay;
                $newDate = $newDate->modify('+ 1 Day');
            }

            if ($form->isValid()) {

                $holidayDemand = new HolidayDemand($user, $days, $comment);

                $this->getDoctrine()->getManager()->persist($holidayDemand);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('employee_manager_holiday_demands');
            }
        }

        return $this->render('/Holiday/DemandCreate.twig', [
            'formular' => $form->createView(),
        ]);
    }

    /**
     * @param DateTimeImmutable $startDate
     * @param DateTimeImmutable $endDate
     * @param HolidayDemand[] $userDemands
     * @return bool
     */
    public function isSameDayAlreadyInAnotherDemandFromSameUser(DateTimeImmutable $startDate, DateTimeImmutable $endDate, array $userDemands): bool
    {
        if (!empty($userDemands)) {
            foreach ($userDemands as $demand) {
                foreach ($demand->getDays() as $day) {
                    if (($day->getDate() >= $startDate && $day->getDate() <= $endDate) && $demand->getStatus() !== HolidayDemand::STATUS_REFUSED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param DateTimeImmutable $startDate
     * @param DateTimeImmutable $endDate
     * @return bool
     */
    public function isStartDateBeforeEndDate(DateTimeImmutable $startDate, DateTimeImmutable $endDate): bool
    {
        if ($startDate <= $endDate) {
            return true;
        }
        return false;
    }

    public function isStartDateTodayOrLaterThanToday(DateTimeImmutable $startDate, DateTimeImmutable $today): bool
    {
        if ($startDate >= $today) {
            return true;
        }
        return false;

    }

    public function isTooFarInTheFuture(DateTimeImmutable $date): bool
    {
        $today = (new DateTimeImmutable())->setTime(0, 0, 0, 0);
        $limitDate = $today->modify('+ 2 years');
        return $date >= $limitDate;
    }

    public function isMaxDurationRespected(DateTimeImmutable $startDate, DateTimeImmutable $endDate): bool
    {
        $dateDiff = $endDate->sub(new DateInterval('P3M'));
        return ($dateDiff < $startDate);
    }
}
