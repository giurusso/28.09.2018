<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 04.09.2018
 * Time: 14:31
 */

namespace App\Controller\Security;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class Logout extends AbstractController
{

    /**
     * @Route("/logout", name="logout")
     * Security("is authentified anonymously')")
     */
    public function __invoke()
    {

    }
}
