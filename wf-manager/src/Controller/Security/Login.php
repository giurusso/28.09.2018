<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 04.09.2018
 * Time: 14:31
 */

namespace App\Controller\Security;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class Login extends AbstractController
{

    /**
     * @Route("/login", name="login")
     */
    public function __invoke(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('/Security/Login.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }
}
