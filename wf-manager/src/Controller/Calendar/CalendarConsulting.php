<?php

namespace App\Controller\Calendar;



use App\ControllerHelpers\DayData;
use App\Entity\Article;
use App\Entity\Day;
use App\Entity\HolidayDemand;
use App\Entity\User;
use App\ControllerHelpers\CalendarData;
use App\Repository\ArticleRepository;

use App\Repository\DayRepository;
use App\Repository\HolidayDemandRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Flex\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class CalendarConsulting extends AbstractController
{

    /**
     * @Route("/manager/rh/calendar/{year}/{month}", name="calendar")
     * @Security("is_granted('ROLE_MANAGER') or is_granted('ROLE_DIRECTOR') or is_granted('ROLE_ADMINISTRATOR')")
     * @param HolidayDemandRepository $holidayDemandRepository
     * @param DayRepository $dayRepository
     * @param $year
     * @param int $month
     * @return Response
     * @throws \Exception
     */
    public function __invoke(HolidayDemandRepository $holidayDemandRepository, DayRepository $dayRepository, $year, int $month)
    {
        $allDays = $dayRepository->findAll();
        $userallDemands = $holidayDemandRepository->findAll();
        $calendar = new CalendarData($year, $month, $userallDemands, false, $allDays);
        return $this->render('/Calendar/CalendarConsulting.twig',[
            'Calendar' => $calendar,
        ]);
    }

    /**
     * @Route("/manager/rh/calendar", name="calendar_redirect")
     */
    public function redirectToCalendar()
    {
        $year = date('Y');
        $month = date('m');

        return $this->redirectToRoute('calendar', ['year' => $year, 'month' => $month]);
    }
}

