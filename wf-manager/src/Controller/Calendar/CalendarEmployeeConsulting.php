<?php

namespace App\Controller\Calendar;


use App\ControllerHelpers\CalendarData;
use App\Repository\DayRepository;
use App\Repository\HolidayDemandRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class CalendarEmployeeConsulting extends AbstractController
{

    /**
     * @Route("/manager/employee/calendar/{year}/{month}", name="employee_calendar", requirements={"id" = "\d+"})
     * @Security("is_granted('ROLE_EMPLOYEE')")
     * @param HolidayDemandRepository $holidayDemandRepository
     * @param DayRepository $dayRepository
     * @param int $year
     * @param int $month
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(HolidayDemandRepository $holidayDemandRepository, DayRepository $dayRepository, int $year, int $month)
    {
        $allDays = $dayRepository->findAll();
        // la variable $currentUser est de type SecurityUser
        $securityUser = $this->getUser();
        $user = $securityUser->getUser();
        $userDemands = $holidayDemandRepository->findByUser($user);

        $calendar = new CalendarData($year, $month, $userDemands, true, $allDays);

        return $this->render('Calendar/CalendarEmployeeConsulting.twig',[
            'Calendar' => $calendar,
        ]);
    }


    /**
     * @Route("/manager/employee/calendar", name="calendar_employee_redirect")
     */
    public function redirectToCalendar()
    {
        $year = date('Y');
        $month = date('m');

        return $this->redirectToRoute('employee_calendar', ['year' => $year, 'month' => $month]);
    }
}