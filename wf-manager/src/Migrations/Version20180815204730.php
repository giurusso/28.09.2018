<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180815204730 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE day (id INT AUTO_INCREMENT NOT NULL, holiday_demand_id INT NOT NULL, requested_day DATE NOT NULL, start_time TIME DEFAULT NULL, end_time TIME DEFAULT NULL, extra_hours INT DEFAULT NULL, blocked TINYINT(1) NOT NULL, status SMALLINT NOT NULL, holiday_modality VARCHAR(255) NOT NULL, INDEX IDX_E5A02990C5378415 (holiday_demand_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE holiday_demand (id INT AUTO_INCREMENT NOT NULL, employee_id INT NOT NULL, created_date DATETIME NOT NULL, status SMALLINT NOT NULL, INDEX IDX_47A4945F8C03F15C (employee_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, car_pooling_participant TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, image VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE day ADD CONSTRAINT FK_E5A02990C5378415 FOREIGN KEY (holiday_demand_id) REFERENCES holiday_demand (id)');
        $this->addSql('ALTER TABLE holiday_demand ADD CONSTRAINT FK_47A4945F8C03F15C FOREIGN KEY (employee_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE day DROP FOREIGN KEY FK_E5A02990C5378415');
        $this->addSql('ALTER TABLE holiday_demand DROP FOREIGN KEY FK_47A4945F8C03F15C');
        $this->addSql('DROP TABLE day');
        $this->addSql('DROP TABLE holiday_demand');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE article');
    }
}
